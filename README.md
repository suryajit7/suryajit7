# Hello World, I'm <a href="https://www.linkedin.com/in/suryajit7/">Suryajit</a> 👋

<p align= 'left'> Software Professional with more than 8 years of experience in IT. I'm a self taught developer and I have good experience with Java & Spring Framework based applications.<br> 
I like solving complex problems and my github repositories majorly consists of projects implementing my own ideas or coding practices.<br>
I also write about tech solutions in my <a href="medium.com/@suryajit7">personal blog</a> which are less talked about but are often needed by Devs in their daily routine. Tweet me <a href="https://twitter.com/suryajit7"> @suryajit7 </a> for blog on any complex topic. <a href="https://twitter.com/intent/tweet?text=Hi%20@Suryajit7,%20Could%20you%20write%20an%20article%20on:%20"><img src="https://img.shields.io/twitter/url?style=social&url=https%3A%2F%2Fgithub.com%2Fryo-ma%2Fgithub-profile-trophy"/></a>
</p>

<body align="center">
<table>
<tr>
<td>
    <img src ="https://gpvc.arturio.dev/suryajit7" /><br><br>
      I'm currently working on the below projects:
      <ul>
        <li>Springfield: Spring based Full-Fledged Enterprise Level Automation Framework</li>
        <li>Hanayama (花山): Performance Suite</li>
        <li>Mangekyō Sharingan (万華鏡写輪眼): Visual Assertion</li>
      </ul>
    </td>
<td class="tg-0lax" rowspan="2">
<br>
<img src="https://media.giphy.com/media/RMwgs5kZqkRyhF24KK/giphy.gif" align="center">
<br>
<br>
</td>
</tr>
<tr>
<td>
<h4>Connect with me:</h4>
<a href="https://twitter.com/suryajit7">
<img src="images/twitter.gif" align="center" width="44px" height="44px"/>
</a>
<a href="https://calendly.com/suryajit7/30min">
<img src="images/animation_640_l655ynw8.gif" align="center" width="60px" height="60px"/>
</a>
<a href="https://suryajit7.github.io/">
<img src="images/animation_200_l653v6cy.gif" align="center" width="60px" height="60px"/>
</a>
<a href="mailto:suryajit7@gmail.com">
<img src="images/gmail.gif" align="center" width="50px" height="50px"/>
</a>
<a href="https://www.linkedin.com/in/suryajit7/">
<img src="images/LINKEDIN_ICON_TRANSPARENT_1080.gif" align="center" width="57px" height="57px"/>
</a><br>
</td>
</tr>
</table>
</body>

<p align="center">
  <br><img src="https://github.com/suryajit7/suryajit7/blob/main/snake.svg" width="450px">
</p>

<p align='center'>
  <a href="https://www.linkedin.com/in/suryajit7/">
    <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" />
  </a>&nbsp;&nbsp;
  <a href="mailto:suryajit7@gmail.com">
    <img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" />        
  </a>&nbsp;&nbsp;
</p>

<p align='center'>
    <a href="https://github-readme-stats.vercel.app/api?username=suryajit7&show_icons=true&count_private=true"><img src="https://github-readme-stats.vercel.app/api?username=suryajit7&show_icons=true&count_private=true&theme=dark&include_all_commits=true&custom_title=My GitHub Stats&show_owner=true&layout=compact" />&nbsp;</a>
    <a href="http://github-readme-streak-stats.herokuapp.com?user=suryajit7&theme=dark&date_format=M%20j%5B%2C%20Y%5D&currStreakLabel=DBDDD7"><img src="http://github-readme-streak-stats.herokuapp.com?user=suryajit7&theme=dark&layout=compact&date_format=M%20j%5B%2C%20Y%5D&currStreakLabel=DBDDD7"/></a><br>
    <a href="https://github-readme-stats.vercel.app/api?username=suryajit7&show_icons=true&count_private=true"><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=suryajit7&layout=compact&theme=dark"/></a>
</p>

<summary>:four_leaf_clover: Technical Skills  </summary>

<a href="https://www.java.com/en/" target="_blank"><img align="center" alt="Java" width="25px" src="https://seeklogo.com/images/J/java-logo-7F8B35BAB3-seeklogo.com.png"/>&nbsp;&nbsp;</a>
<a href="https://spring.io/" target="_blank"><img align="center" alt="Spring" width="30px" src="https://seeklogo.com/images/S/spring-logo-9A2BC78AAF-seeklogo.com.png" />&nbsp;&nbsp;</a>
<a href="https://git-scm.com/" target="_blank"><img align="center" alt="git" width="26px" src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" />&nbsp;&nbsp;</a>
<a href="https://www.mysql.com/" target="_blank"><img align="center" alt="MySQL" width="45px" height="45px" src="https://www.freepnglogos.com/uploads/logo-mysql-png/logo-mysql-mysql-logo-png-transparent-svg-vector-bie-supply-2.png" />&nbsp;</a>
<a href="https://www.mongodb.com/" target="_blank"><img align="center" alt="MongoDB" width="30px" src="images/mongodb.png" />&nbsp;&nbsp;</a>
<a href="https://graphql.org/" target="_blank"><img align="center" alt="GraphQL" width="25px" src="https://seeklogo.com/images/G/graphql-logo-97CBBB6D51-seeklogo.com.png"/>&nbsp;&nbsp;</a>

<a href="https://www.postman.com/" target="_blank"><img align="center" alt="Postman" width="30px" src="images/postman-icon.svg" />&nbsp;&nbsp;</a>
<a href="https://gradle.org/" target="_blank"><img align="center" alt="Gradle" width="30px" src="images/gradle_icon.svg" />&nbsp;</a>
<a href="https://junit.org/junit5/" target="_blank"><img align="center" alt="JUnit" width="30px" src="https://asset.brandfetch.io/idD7RfhCFS/id3KSPzOxb.png" />&nbsp;&nbsp;</a>
<a href="http://www.seleniumhq.org/" target="_blank"><img align="center" alt="Selenium" width="30px" src="https://seeklogo.com/images/S/selenium-logo-A1B53CEFB0-seeklogo.com.png" />&nbsp;&nbsp;</a>
<a href="http://appium.io/" target="_blank"><img align="center" alt="Appium" width="30px" src="https://seeklogo.com/images/A/appium-logo-2AB368AF4A-seeklogo.com.png" />&nbsp;&nbsp;</a>
<a href="http://rest-assured.io/" target="_blank"><img align="center" alt="REST-assured" width="30px" src="https://avatars.githubusercontent.com/u/19369327?s=200&v=4" />&nbsp;&nbsp;</a>

<a href="https://www.jenkins.io/" target="_blank"><img align="center" alt="Jenkins" width="30px" src="images/jenkins_logo.svg" />&nbsp;&nbsp;</a>
<a href="https://aws.amazon.com/" target="_blank"><img align="center" alt="AWS" width="40px" src="images/aws.png" />&nbsp;&nbsp;</a>
<a href="https://gitlab.com/aleksandr-kotlyar" target="_blank"><img align="center" alt="GitLab" width="30px" src="https://about.gitlab.com/images/devops-tools/gitlab-logo.svg" />&nbsp;&nbsp;</a>
<a href="https://github.com/aleksandr-kotlyar" target="_blank"><img align="center" alt="GitHub" width="40px" src="https://www.iconninja.com/files/604/580/1001/github-development-code-coding-program-programming-icon.svg" />&nbsp;</a>
<a href="https://www.docker.com/" target="_blank"><img align="center" alt="Docker" width="30px" src="https://seeklogo.com/images/D/docker-logo-CF97D0124B-seeklogo.com.png" />&nbsp;&nbsp;</a>

<a href="https://www.jetbrains.com/idea/" target="_blank"><img align="center" alt="IntelliJ IDEA" width="30px" src="https://seeklogo.com/images/I/intellij-idea-logo-F0395EF783-seeklogo.com.png" />&nbsp;&nbsp;</a>
<a href="https://www.salesforce.com/in/" target="_blank"><img align="center" alt="Salesforce" width="40px" src="images/salesforce-2.svg" />&nbsp;&nbsp;</a>
<a href="https://studio3t.com/" target="_blank"><img align="center" alt="Studio-3T" width="30px" src="https://avatars.githubusercontent.com/u/25457492?s=200&v=4" />&nbsp;</a>
<a href="https://www.atlassian.com/" target="_blank"><img align="center" alt="Atlassian" width="40px" src="https://avatars.githubusercontent.com/u/43281909?s=200&v=4" />&nbsp;</a>
<a href="https://developer.android.com/studio" target="_blank"><img align="center" alt="Android Studio" width="53px" src="images/android-studio_logo.png" />&nbsp;</a>
<br />
